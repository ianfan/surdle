'use strict';

window.onload = () => {
  const COUNTER_START = 19075;
  const MAX_GUESS_COUNT = 6;
  const MAX_GUESS = 2643;
  const WIDTH = 5;
  const LAST_DIGIT = WIDTH - 2;

  const STORAGE = window.localStorage;

  let hardMode = localStorage.getItem('hardMode') === 'y';
  // document.getElementsByName('hard-mode')[0].checked = hardMode;

  let darkTheme = localStorage.getItem('darkTheme');
  if (darkTheme !== null) darkTheme = darkTheme === 'y';
  else darkTheme = window.matchMedia('(prefers-color-scheme: dark)');
  document.documentElement.classList.toggle('dark-theme', darkTheme);
  document.getElementsByName('dark-theme')[0].checked = darkTheme;

  let highContrast = localStorage.getItem('highContrast') === 'y';
  document.documentElement.classList.toggle('high-contrast', highContrast);
  document.getElementsByName('high-contrast')[0].checked = highContrast;

  document.getElementsByTagName('menu')[0].onchange = e => {
    if (e.target.name === 'hard-mode') {
      if (guesses.length === 0) {
        hardMode = e.target.checked;
        localStorage.setItem('hardMode', hardMode ? 'y' : 'n');
      }
    } else if (e.target.name === 'dark-theme') {
      darkTheme = e.target.checked;
      document.documentElement.classList.toggle('dark-theme', darkTheme);
      localStorage.setItem('darkTheme', darkTheme ? 'y' : 'n');
    } else if (e.target.name === 'high-contrast') {
      highContrast = e.target.checked;
      document.documentElement.classList.toggle('high-contrast', highContrast);
      localStorage.setItem('highContrast', highContrast ? 'y' : 'n');
    }
  }

  function evaluate(radicand) {
    const result = Math.sqrt(radicand);
    const truncation = result.toString().slice(0, WIDTH);
    return truncation.length === WIDTH ? truncation : result.toPrecision(WIDTH - 1);  // -1 for decimal point
  }

  function disable(row) {
    row.getElementsByTagName('input')[0].readOnly = true;
    row.getElementsByTagName('button')[0].disabled = true;
  }

  function enable(row) {
    const inputEl = row.getElementsByTagName('input')[0];
    inputEl.readOnly = false;
    inputEl.focus();
    row.getElementsByTagName('button')[0].disabled = false;
  }

  function focusRow() {
    for (let i = 0; i < MAX_GUESS_COUNT; ++i) {
      const inputEl = mainEl.children[i].getElementsByTagName('input')[0];
      if (!inputEl.readOnly) {
        inputEl.focus();
        return;
      }
    }
  }

  function resolve(radicand, rowEl) {
    const guess = evaluate(radicand);

    let win = true;
    const status = [];
    const unmatched = [];
    for (let i = 0; i < WIDTH; ++i) {
      if (guess[i] === target[i]) {
        status.push('=');
      } else {
        status.push(null);
        unmatched.push(target[i]);
        win = false;
      }
    }

    for (let i = 0; i < WIDTH; ++i) {
      if (status[i] !== '=') {
        const idx = unmatched.indexOf(guess[i]);
        if (idx > -1) {
          unmatched.splice(idx, 1);
          status[i] = '~';
        } else {
          status[i] = '_';
        }
      }
    }

    const digits = rowEl.getElementsByClassName('result');
    for (let i = 0; i < WIDTH; ++i) {
      digits[i].textContent = guess[i];
      if (status[i] === '=') digits[i].classList.add('correct');
      else if (status[i] === '~') digits[i].classList.add('present');
      else if (status[i] === '_') digits[i].classList.add('absent');
      else throw new Error();
    }

    disable(rowEl);
    rowEl.classList.add('resolved');

    // document.getElementsByName('hard-mode')[0].disabled = true;

    return win;
  }

  function updateStats() {
    document.getElementById('played').textContent = played;
    document.getElementById('win-perc').textContent = (
      played > 0 ? Math.round(100 * wins / played) : 0);
    document.getElementById('streak').textContent = streak;
    document.getElementById('max-streak').textContent = maxStreak;
  }

  const MODULUS = 2647;
  const MULTIPLIER = 37;
  const MS_IN_DAY = 8.64e7;
  const now = new Date();
  const counter = Math.floor((now - now.getTimezoneOffset()) / MS_IN_DAY);
  let answer = 1;
  for (let i = 0; i < counter; ++i) {
    answer += MODULUS;
    while (answer > MAX_GUESS + 1) answer = answer * MULTIPLIER % MODULUS;
  }
  --answer;
  const target = evaluate(answer);

  let played = +STORAGE.getItem('played');
  let wins = +STORAGE.getItem('wins');
  let streak = +STORAGE.getItem('streak');
  let maxStreak = +STORAGE.getItem('maxStreak');
  let distribution = (JSON.parse(STORAGE.getItem('distribution'))
                        || new Array(MAX_GUESS_COUNT).fill(0));
  updateStats();

  const guesses = (counter === +STORAGE.getItem('last')
                   ? JSON.parse(STORAGE.getItem('guesses'))
                   : []);
  STORAGE.setItem('last', counter);
  STORAGE.setItem('guesses', JSON.stringify(guesses));

  const alreadyPlayed = (guesses.length === MAX_GUESS_COUNT
                         || guesses[guesses.length - 1] === answer);
  const shareButton = document.getElementById('share');
  if (alreadyPlayed) shareButton.className = '';

  const mainEl = document.getElementsByTagName('main')[0];
  const rowEls = mainEl.children;
  const templateRowEl = document.getElementsByTagName('form')[0];
  templateRowEl.getElementsByTagName('input')[0].value = '';
  templateRowEl.remove();

  for (let i = 0; i < MAX_GUESS_COUNT; ++i) {
    const rowEl = templateRowEl.cloneNode(true);
    if (i > guesses.length - alreadyPlayed) disable(rowEl);
    mainEl.appendChild(rowEl);
  }

  mainEl.offsetWidth;  // force reflow

  let win = false;
  for (let i = 0; i < MAX_GUESS_COUNT; ++i) {
    const rowEl = rowEls[i];
    if (i < guesses.length) {
      const radicand = guesses[i];
      rowEl.children[WIDTH - 1].children[0].value = radicand % 10;

      let n = radicand;
      for (let d = LAST_DIGIT; d > 0; --d) {
        n = Math.floor(n / 10);
        if (n === 0) break;
        rowEl.children[d].children[0].textContent = n % 10;
      }

      rowEl.classList.add('presolved');
      win = resolve(radicand, rowEl);
    }
  }

  if (win) {
    setTimeout(() => {
      document.getElementById('stats').parentNode.open = true;
    }, 600);
  } else {
    focusRow();
  }

  document.body.onclick = e => {
    let setFocus = true;
    if (e.target.className === 'exit') {
      e.target.parentNode.parentNode.open = false;
    } else {
      for (let details of document.getElementsByTagName('details')) {
        if (!details.contains(e.target)) details.open = false;
        else if (!(e.target == details.children[0] && details.open)) setFocus = false;
      }
    }

    if (setFocus) focusRow();
  };

  mainEl.addEventListener('beforeinput', e => {
    if (e.inputType === 'insertLineBreak') return;

    e.preventDefault();
    e.target.setCustomValidity('');

    const rowEl = e.target.parentNode.parentNode;
    const digits = rowEl.getElementsByClassName('guess');
    if (e.inputType === 'insertText') {
      if ('0' <= e.data && e.data <= '9' && digits[1].textContent === '') {
        if (!(e.target.value === '0' && digits[LAST_DIGIT].textContent === '')) {
          for (let i = 1; i < LAST_DIGIT; ++i) {
            digits[i].textContent = digits[i + 1].textContent;
          }
          digits[LAST_DIGIT].textContent = e.target.value;
        }
        e.target.value = e.data;
      }
    } else if (e.inputType === 'deleteContentBackward') {
      e.target.value = digits[LAST_DIGIT].textContent;
      for (let i = LAST_DIGIT; i > 1; --i) {
        digits[i].textContent = digits[i - 1].textContent;
      }
      digits[1].textContent = '';
    }
  });

  mainEl.onsubmit = e => {
    const digitEls = e.target.getElementsByClassName('guess');
    const inputEl = digitEls[WIDTH - 1];
    if (inputEl.value === '') return false;

    let radicand = 0;
    for (let i = 1; i < WIDTH - 1; ++i) {
      radicand = radicand * 10 + +digitEls[i].textContent;
    }
    radicand = radicand * 10 + +inputEl.value;

    if (!(0 <= radicand && radicand <= MAX_GUESS)) {
      inputEl.setCustomValidity(`0 ≤ answer ≤ ${MAX_GUESS}`);
      inputEl.reportValidity();
      return false;
    }

    guesses.push(radicand);
    STORAGE.setItem('guesses', JSON.stringify(guesses));
    win = resolve(radicand, e.target);

    if (!win && guesses.length < MAX_GUESS_COUNT) {
      enable(rowEls[guesses.length]);
    } else {
      STORAGE.setItem('played', ++played);
      if (win) {
        STORAGE.setItem('wins', ++wins);

        maxStreak = Math.max(maxStreak, ++streak);
        STORAGE.setItem('maxStreak', maxStreak);

        ++distribution[guesses.length];
        STORAGE.setItem('distribution', JSON.stringify(distribution));
      } else {
        streak = 0;
      }
      STORAGE.setItem('streak', streak);

      updateStats();

      shareButton.className = '';
      setTimeout(() => {
        document.getElementById('stats').parentNode.open = true;
      }, 2000);
    }

    return false;
  };

  shareButton.onclick = () => {
    let output = `Surdle ${counter - COUNTER_START} ${win ? guesses.length : "X"}/${MAX_GUESS_COUNT}\n`;
    for (let row of rowEls) {
      if (!row.classList.contains('resolved')) break;

      output += '\n';
      for (let digitEl of row.getElementsByClassName('result')) {
        if (digitEl.classList.contains('correct')) {
          output += '🟩';
        } else if (digitEl.classList.contains('present')) {
          output += '🟨';
        } else if (digitEl.classList.contains('absent')) {
          output += darkTheme ? '⬛' : '⬜';
        } else {
          throw new Error();
        }
      }
    }
    navigator.clipboard.writeText(output);
    shareButton.textContent = 'COPIED';
  };
}
